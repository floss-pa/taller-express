const Sequelize = require("sequelize");

const connection = new Sequelize("postgres", "postgres", "docker", {
  dialect: "postgres",
  host: "localhost"
});

module.exports = connection;
