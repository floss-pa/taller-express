const Sequelize = require("sequelize");
const connection = require("./sequelize");

const User = connection.define("User", {
  username: Sequelize.STRING,
  password: Sequelize.STRING
});

module.exports = User;
