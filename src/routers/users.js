const Express = require("express");
const Users = require("../model/User");
const router = Express.Router();
const bcrypt = require("bcrypt");

router.get("/", async function(req, res) {
  const userList = await Users.findAll();
  res.render("users", { users: userList });
});

router.get("/:id", async function(req, res) {
  const userId = req.params.id;
  const user = await Users.findOne({ id: userId });
  if (user) {
    res.json(user);
  } else {
    res.status(404);
    res.send("No encontré nada con ese ID");
  }
});

router.post("/", async (req, res) => {
  const { username, password } = req.body;
  if (!username) {
    res.status(402);
    res.send("Falta 'name'");
  } else if (!password) {
    res.status(402);
    res.send("Falta 'password'");
  } else {
    const salt = await bcrypt.genSalt(10);
    const saltedPassword = await bcrypt.hash(password, salt);
    const savedUser = await Users.create({
      username,
      password: saltedPassword
    });
    res.json(savedUser);
  }
});

module.exports = router;
