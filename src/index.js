const Express = require("express");
const BodyParser = require("body-parser");
const UserRouter = require("./routers/users");
const app = Express();
const connection = require("./model/sequelize");

app.set("view engine", "pug");
app.set("views", __dirname + "/views");

app.use(BodyParser.json());

app.get("/", function(req, res) {
  res.render("index");
});

app.use("/usuarios", UserRouter);

const PORT = 8080;
connection.sync().then(() => {
  console.log("Conectado a la DB");

  app.listen(PORT, function() {
    console.log(`Escuchando en puesto ${PORT}`);
  });
});
